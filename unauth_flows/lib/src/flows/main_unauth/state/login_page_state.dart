import 'package:authentication/authentication.dart';
import 'package:flutter/foundation.dart';

class LoginPageState extends ChangeNotifier {
  LoginPageState(this._authInteractor);

  final AuthInteractor _authInteractor;

  bool _isFetching = false;

  get isFetching => _isFetching;
  set isFetching(bool isFetching) {
    _isFetching = isFetching;
    notifyListeners();
  }

  Future<void> login(String email, String password) async {
    isFetching = true;
    await _authInteractor.login(email, password);
    isFetching = false;
  }
}
