import 'package:flutter/foundation.dart';

class SignupPageState extends ChangeNotifier {
  String _nickname;
  String _email;
  String _password;

  setNickname(String nickname) {
    _nickname = nickname;
    notifyListeners();
  }

  setEmail(String email) {
    _email = email;
    notifyListeners();
  }

  setPassword(String password) {
    _password = password;
    notifyListeners();
  }
}
