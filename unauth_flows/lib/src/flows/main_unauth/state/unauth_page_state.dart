import 'package:flutter/foundation.dart';

class UnauthPageState extends ChangeNotifier {
  UnauthPageState();
  int _currentPage = 0;

  int get currentPage => _currentPage;
  set currentPage(int currentPage) {
    _currentPage = currentPage;
    notifyListeners();
  }
}
