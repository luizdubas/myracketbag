import 'package:design_system/design_system.dart';
import 'package:flutter/material.dart';

class SignupPage extends StatefulWidget {
  SignupPage({Key key}) : super(key: key);

  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final _nicknameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.transparent,
      body: Container(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: ProjectMetrics.marginLarge,
          ),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding:
                      const EdgeInsets.only(top: ProjectMetrics.marginLarge),
                  child: LightTextField(
                    controller: _nicknameController,
                    hintText: "apelido",
                    prefixIcon: Icon(
                      Icons.account_circle,
                      color: ProjectColors.lightEnabledIcon,
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: ProjectMetrics.marginDefault),
                  child: LightTextField(
                    controller: _emailController,
                    hintText: "e-mail",
                    keyboardType: TextInputType.emailAddress,
                    prefixIcon: Icon(
                      Icons.email,
                      color: ProjectColors.lightEnabledIcon,
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: ProjectMetrics.marginDefault),
                  child: LightTextField(
                    controller: _passwordController,
                    hintText: "senha",
                    obscureText: true,
                    prefixIcon: Icon(
                      Icons.https,
                      color: ProjectColors.lightEnabledIcon,
                    ),
                    validator: (pass) {
                      if (pass.length < 8) {
                        return "Senha muito curta (min.: 8 caracteres)";
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: ProjectMetrics.marginLarge),
                  child: TabAction(
                    title: "REGISTRAR",
                    onTap: () {
                      print(">> registrar");
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
