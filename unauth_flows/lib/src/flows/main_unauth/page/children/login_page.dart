import 'package:design_system/design_system.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../state/login_page_state.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  _LoginPageState();

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final _loginState = Provider.of<LoginPageState>(context);
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.transparent,
      body: Container(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: ProjectMetrics.marginLarge,
          ),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding:
                      const EdgeInsets.only(top: ProjectMetrics.marginLarge),
                  child: LightTextField(
                    controller: _emailController,
                    hintText: "e-mail",
                    keyboardType: TextInputType.emailAddress,
                    prefixIcon: Icon(
                      Icons.email,
                      color: ProjectColors.lightEnabledIcon,
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: ProjectMetrics.marginDefault),
                  child: LightTextField(
                    controller: _passwordController,
                    hintText: "senha",
                    obscureText: true,
                    prefixIcon: Icon(
                      Icons.https,
                      color: ProjectColors.lightEnabledIcon,
                    ),
                    validator: (pass) {
                      if (pass.length < 8) {
                        return "Senha muito curta (min.: 8 caracteres)";
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: ProjectMetrics.marginLarge),
                  child: TabAction(
                    title: "LOGIN",
                    enabled: !_loginState.isFetching,
                    onTap: () async {
                      if (_formKey.currentState.validate()) {
                        await _loginState
                            .login(
                          _emailController.text,
                          _passwordController.text,
                        )
                            .catchError((err) {
                          _showErrorInSnackBar(err.message);
                        });
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showErrorInSnackBar(String error) {
    FocusScope.of(context).requestFocus(FocusNode());
    _scaffoldKey.currentState?.removeCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(
        error,
        textAlign: TextAlign.left,
        style: ProjectTextStyles.roboto14RegularLight,
      ),
      backgroundColor: Colors.red,
      duration: Duration(seconds: 3),
    ));
  }
}
