import 'package:design_system/design_system.dart';
import 'package:design_system/theme/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../state/unauth_page_state.dart';
import 'children/login_page.dart';
import 'children/signup_page.dart';

class MainUnauthPage extends StatefulWidget {
  MainUnauthPage({Key key}) : super(key: key);

  @override
  _MainUnauthPageState createState() => _MainUnauthPageState();
}

class _MainUnauthPageState extends State<MainUnauthPage> {
  final _tabBarKey = GlobalKey();

  final PageController _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    final _state = Provider.of<UnauthPageState>(context);
    print(">> State == ${_state.currentPage}");
    return Scaffold(
      backgroundColor: ProjectColors.backgroundColor,
      body: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll) {
          overscroll.disallowGlow();
          return false;
        },
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.transparent,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                    top: 144,
                  ),
                  child: Row(
                    children: <Widget>[
                      SizedBox(
                        width: ProjectMetrics.marginLarge,
                        child: Container(
                          color: ProjectColors.accentColor,
                          height: 3.0,
                        ),
                      ),
                      Expanded(child: _buildMenuBar(context, _state)),
                      SizedBox(
                        width: ProjectMetrics.marginLarge,
                        child: Container(
                          color: ProjectColors.accentColor,
                          height: 3.0,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: PageView(
                    controller: _pageController,
                    onPageChanged: (i) {
                      _state.currentPage = i;
                    },
                    children: <Widget>[
                      ConstrainedBox(
                        constraints: const BoxConstraints.expand(),
                        child: LoginPage(),
                      ),
                      ConstrainedBox(
                        constraints: const BoxConstraints.expand(),
                        child: SignupPage(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildMenuBar(BuildContext context, UnauthPageState state) {
    return Container(
      key: _tabBarKey,
      height: 32.0,
      decoration: BoxDecoration(
        gradient: ProjectColors.tabBackgroundGradient,
        borderRadius: BorderRadius.all(Radius.circular(16.0)),
      ),
      child: CustomPaint(
        painter: TabIndicationPainter(
          dxTarget: (_tabWidth(context) / 2) - 16,
          pageController: _pageController,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: _onLoginButtonPress,
                child: Text(
                  "Login",
                  style: state.currentPage == 0
                      ? ProjectTextStyles.roboto20MediumLight
                      : ProjectTextStyles.roboto20MediumDark,
                ),
              ),
            ),
            Expanded(
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: _onSignUpButtonPress,
                child: Text(
                  "Registrar",
                  style: state.currentPage == 1
                      ? ProjectTextStyles.roboto20MediumLight
                      : ProjectTextStyles.roboto20MediumDark,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _onLoginButtonPress() {
    _pageController.animateToPage(0,
        duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  _onSignUpButtonPress() {
    _pageController?.animateToPage(1,
        duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  double _tabWidth(BuildContext context) =>
      MediaQuery.of(context).size.width - ProjectMetrics.marginLarge * 2;
}
