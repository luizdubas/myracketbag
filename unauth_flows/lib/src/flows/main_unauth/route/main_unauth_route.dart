import 'package:authentication/authentication.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import '../page/main_unauth_page.dart';
import '../state/login_page_state.dart';
import '../state/unauth_page_state.dart';

class MainUnauthRoute extends StatelessWidget {
  const MainUnauthRoute({Key key, this.authInteractor}) : super(key: key);

  final AuthInteractor authInteractor;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ListenableProvider<UnauthPageState>.value(
          listenable: UnauthPageState(),
        ),
        ListenableProvider<LoginPageState>.value(
          listenable: LoginPageState(authInteractor),
        ),
      ],
      child: MainUnauthPage(),
    );
  }
}
