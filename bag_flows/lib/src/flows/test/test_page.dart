import 'package:design_system/theme/metrics.dart';
import 'package:flutter/material.dart';
import 'package:design_system/design_system.dart';
import 'package:flutter/src/widgets/basic.dart';
import 'package:flutter/widgets.dart';

typedef FutureCallback = Future<void> Function();

class TestPage extends StatelessWidget {
  const TestPage({Key key, this.onLogout}) : super(key: key);

  final FutureCallback onLogout;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ProjectColors.backgroundColor,
      child: SafeArea(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                top: ProjectMetrics.marginDefault,
              ),
              child: TitleText(
                name: 'Luiz',
                rightLabel: 'Sair',
                rightAction: onLogout,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: ProjectMetrics.marginVertical,
                left: ProjectMetrics.marginDefault,
                right: ProjectMetrics.marginDefault,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      'Essa é sua raqueteira:',
                      style: ProjectTextStyles.roboto20RegularDark,
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: ListView(
                padding: const EdgeInsets.only(
                  bottom: ProjectMetrics.marginVertical,
                ),
                shrinkWrap: false,
                children: [
                  _createRacketCard(
                    "EZONE 98H",
                    "53 lbs",
                    "Alu Power Rough 16L",
                    "Alu Power Rough 16L",
                  ),
                  _createRacketCard(
                    "EZONE 98H",
                    "46 lbs",
                    "Alu Power",
                    "Alu Power",
                  ),
                  _createRacketCard(
                    "EZONE 98H",
                    "53 lbs",
                    "Alu Power Rough 16L",
                    "Alu Power Rough 16L",
                  ),
                  _createRacketCard(
                    "EZONE 98H",
                    "46 lbs",
                    "Alu Power",
                    "Alu Power",
                  ),
                  _createRacketCard(
                    "EZONE 98H",
                    "53 lbs",
                    "Alu Power Rough 16L",
                    "Alu Power Rough 16L",
                  ),
                  _createRacketCard(
                    "EZONE 98H",
                    "46 lbs",
                    "Alu Power",
                    "Alu Power",
                  ),
                ],
              ),
            ),
            // DialogAction(
            //   title: "LOGAR",
            //   enabled: true,
            //   onTap: () {
            //     print(">> Logar");
            //   },
            // ),
            // TabAction(
            //   title: "LOGOUT",
            //   enabled: true,
            //   onTap: () {
            //     print(">> Logout");
            //     onLogout();
            //   },
            // ),
            BottomAction(
              title: "ADICIONAR RAQUETE",
              enabled: true,
              onTap: () {
                showDialog(
                  context: context,
                  barrierDismissible: true,
                  builder: _createDialog,
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _createDialog(BuildContext context) {
    return CardDialog(
      child: Padding(
        padding: const EdgeInsets.only(top: ProjectMetrics.marginDefault),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                top: ProjectMetrics.marginBetween,
                left: ProjectMetrics.marginDefault,
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      "Raquete",
                      style: ProjectTextStyles.roboto24MediumLight,
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: ProjectMetrics.marginVertical,
                left: ProjectMetrics.marginDefault,
                right: ProjectMetrics.marginDefault,
                bottom: ProjectMetrics.marginVertical,
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: LightTextField(
                      keyboardType: TextInputType.text,
                      hintText: "apelido",
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: ProjectMetrics.marginDefault,
                right: ProjectMetrics.marginDefault,
                bottom: ProjectMetrics.marginVertical,
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: LightTextField(
                      keyboardType: TextInputType.text,
                      hintText: "modelo",
                    ),
                  )
                ],
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: DialogAction(
                    title: "ADICIONAR",
                    enabled: true,
                    onTap: () {
                      print(">> Raquete adicionada");
                    },
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _createRacketCard(
    String name,
    String tension,
    String main,
    String cross,
  ) {
    return BaseListCard(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                name,
                style: ProjectTextStyles.roboto20MediumLight,
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: ProjectMetrics.marginSlim,
            ),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Padding(
                    padding: const EdgeInsets.only(
                      right: ProjectMetrics.marginSlim,
                    ),
                    child: Text(
                      "Tensão:",
                      style: ProjectTextStyles.roboto14RegularLight,
                    ),
                  ),
                ),
                Expanded(
                  flex: 7,
                  child: Text(
                    tension,
                    style: ProjectTextStyles.roboto14LightLight,
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: ProjectMetrics.marginBetween,
            ),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Padding(
                    padding: const EdgeInsets.only(
                      right: ProjectMetrics.marginSlim,
                    ),
                    child: Text(
                      "Main:",
                      style: ProjectTextStyles.roboto14RegularLight,
                    ),
                  ),
                ),
                Expanded(
                  flex: 7,
                  child: Text(
                    main,
                    style: ProjectTextStyles.roboto14LightLight,
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: ProjectMetrics.marginBetween,
            ),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Padding(
                    padding: const EdgeInsets.only(
                      right: ProjectMetrics.marginSlim,
                    ),
                    child: Text(
                      "Cross:",
                      style: ProjectTextStyles.roboto14RegularLight,
                    ),
                  ),
                ),
                Expanded(
                  flex: 7,
                  child: Text(
                    cross,
                    style: ProjectTextStyles.roboto14LightLight,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
