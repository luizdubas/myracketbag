import 'package:firebase_auth/firebase_auth.dart';

import '../state/auth_state.dart';

abstract class BaseAuthInteractor {
  Stream<AuthState> get authStatusStream;

  Future<String> login(String email, String password);

  Future<String> signUp(String email, String password);

  Future<void> resetPassword(String email);

  Future<void> signOut();

  Future<void> sendEmailVerification();

  Future<bool> isEmailVerified();
}

class AuthInteractor implements BaseAuthInteractor {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Stream<AuthState> get authStatusStream {
    return _firebaseAuth.onAuthStateChanged.skipWhile((user) {
      return user == null;
    }).map(_getAuthStateFrom);
  }

  Future<String> login(String email, String password) async {
    final user = await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    return user.uid;
  }

  Future<String> signUp(String email, String password) async {
    final user = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    return user.uid;
  }

  Future<void> signOut() async {
    await _firebaseAuth.signOut();
  }

  Future<void> resetPassword(String email) async {
    await _firebaseAuth.sendPasswordResetEmail(email: email);
  }

  Future<void> sendEmailVerification() async {
    final user = await _firebaseAuth.currentUser();
    user.sendEmailVerification();
  }

  Future<bool> isEmailVerified() async {
    final user = await _firebaseAuth.currentUser();
    return user.isEmailVerified;
  }

  AuthState _getAuthStateFrom(FirebaseUser user) {
    return user?.uid == null ? AuthState.notLoggedIn : AuthState.loggedIn;
  }
}
