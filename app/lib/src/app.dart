import 'package:authentication/authentication.dart';
import 'package:bag_flows/bag_flows.dart';
import 'package:design_system/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:unauth_flows/unauth_flows.dart';

class MyRacketBagApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BaseAuthInteractor authInteractor = AuthInteractor();
    return MaterialApp(
      theme: projectTheme,
      title: 'My Racket Bag',
      initialRoute: '/',
      routes: {
        '/': (context) => AppPage(
              authInteractor: authInteractor,
            ),
      },
    );
  }
}

class AppPage extends StatelessWidget {
  final BaseAuthInteractor authInteractor;

  AppPage({Key key, @required this.authInteractor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: authInteractor.authStatusStream.distinct(),
      initialData: AuthState.notLoggedIn,
      builder: (
        context,
        status,
      ) {
        return AnimatedSwitcher(
          switchInCurve: Curves.easeInCubic,
          switchOutCurve: Curves.easeOutCubic,
          duration: Duration(milliseconds: 300),
          child: _pageFromState(status.data, context),
        );
      },
    );
  }

  Widget _pageFromState(AuthState state, BuildContext context) {
    switch (state) {
      case AuthState.loggedIn:
        return TestPage(
          onLogout: authInteractor.signOut,
        );
      default:
        return MainUnauthRoute(authInteractor: authInteractor);
    }
  }
}
