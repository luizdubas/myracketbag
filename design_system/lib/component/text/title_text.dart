import 'package:flutter/material.dart';

import '../../theme/metrics.dart';
import '../../theme/text_styles.dart';

class TitleText extends StatelessWidget {
  const TitleText({
    Key key,
    this.name,
    this.rightLabel,
    this.rightAction,
  }) : super(key: key);

  final String name;
  final String rightLabel;
  final VoidCallback rightAction;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: ProjectMetrics.marginDefault,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    "Bem vindo,",
                    style: ProjectTextStyles.roboto24MediumDark,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.left,
                  ),
                ),
                FlatButton(
                  onPressed: rightAction,
                  child: Text(
                    rightLabel,
                    style: ProjectTextStyles.roboto20RegularDark,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.right,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    "$name!",
                    style: ProjectTextStyles.roboto36MediumDark,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
