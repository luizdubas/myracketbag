import 'package:flutter/material.dart';

import '../../theme/colors.dart';
import '../../theme/metrics.dart';

class CardDialog extends StatelessWidget {
  const CardDialog({Key key, this.child}) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      child: Wrap(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              gradient: ProjectColors.cardBackgroundGradient,
              borderRadius: BorderRadius.all(
                Radius.circular(ProjectMetrics.cardRadius),
              ),
              shape: BoxShape.rectangle,
            ),
            child: child,
          ),
        ],
      ),
    );
  }
}
