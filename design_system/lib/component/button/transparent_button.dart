import 'package:flutter/material.dart';

import '../../theme/colors.dart';
import '../../theme/metrics.dart';
import '../../theme/text_styles.dart';

class TransparentButton extends StatelessWidget {
  const TransparentButton({
    Key key,
    @required this.onTap,
    this.title,
    this.enabled = true,
  }) : super(key: key);

  final String title;
  final bool enabled;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      height: ProjectMetrics.buttonBig,
      child: Material(
        shape: Border(
            top: BorderSide(
          color: ProjectColors.borderColor,
          style: BorderStyle.solid,
          width: ProjectMetrics.borderDefault,
        )),
        type: MaterialType.transparency,
        textStyle: ProjectTextStyles.roboto28MediumLight,
        child: InkWell(
          onTap: onTap,
          enableFeedback: enabled,
          child: Padding(
            padding: const EdgeInsets.all(ProjectMetrics.marginDefault),
            child: Text(
              title,
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
