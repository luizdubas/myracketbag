import 'package:flutter/material.dart';

import '../../theme/colors.dart';
import '../../theme/metrics.dart';
import '../../theme/text_styles.dart';

class BottomAction extends StatelessWidget {
  const BottomAction({
    Key key,
    @required this.onTap,
    this.title,
    this.enabled = true,
  }) : super(key: key);

  final String title;
  final bool enabled;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: ProjectColors.cardBackgroundGradient,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(ProjectMetrics.textRadius),
          topRight: Radius.circular(ProjectMetrics.textRadius),
        ),
        shape: BoxShape.rectangle,
      ),
      height: ProjectMetrics.buttonNormal,
      child: Material(
        type: MaterialType.transparency,
        textStyle: ProjectTextStyles.roboto14BoldLight,
        child: InkWell(
          onTap: onTap,
          enableFeedback: enabled,
          child: Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: ProjectMetrics.marginDefault),
            child: Center(
              child: Text(
                title,
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
