import 'package:flutter/material.dart';

import '../../theme/colors.dart';
import '../../theme/metrics.dart';
import '../../theme/text_styles.dart';

class DialogAction extends StatelessWidget {
  const DialogAction({
    Key key,
    @required this.onTap,
    this.title,
    this.enabled = true,
  }) : super(key: key);

  final String title;
  final bool enabled;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ProjectMetrics.buttonBig,
      decoration: BoxDecoration(
        color: ProjectColors.backgroundColor,
        borderRadius: BorderRadius.all(
          Radius.circular(ProjectMetrics.cardRadius),
        ),
        shape: BoxShape.rectangle,
      ),
      child: Material(
        type: MaterialType.transparency,
        textStyle: ProjectTextStyles.roboto20MediumDark,
        child: InkWell(
          borderRadius: BorderRadius.all(
            Radius.circular(ProjectMetrics.cardRadius),
          ),
          onTap: onTap,
          enableFeedback: enabled,
          child: Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: ProjectMetrics.marginDefault),
            child: Center(
              child: Text(
                title,
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
