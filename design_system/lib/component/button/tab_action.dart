import 'package:flutter/material.dart';

import '../../theme/colors.dart';
import '../../theme/metrics.dart';
import '../../theme/text_styles.dart';

class TabAction extends StatelessWidget {
  const TabAction({
    Key key,
    @required this.onTap,
    this.title,
    this.enabled = true,
  }) : super(key: key);

  final String title;
  final bool enabled;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ProjectMetrics.buttonNormal,
      decoration: BoxDecoration(
        gradient: ProjectColors.cardBackgroundGradient,
        borderRadius: BorderRadius.all(
          Radius.circular(ProjectMetrics.tabButtonRadius),
        ),
        shape: BoxShape.rectangle,
      ),
      child: Material(
        type: MaterialType.transparency,
        textStyle: ProjectTextStyles.roboto24MediumLight,
        child: InkWell(
          borderRadius: BorderRadius.all(
            Radius.circular(ProjectMetrics.tabButtonRadius),
          ),
          onTap: onTap,
          enableFeedback: enabled,
          child: Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: ProjectMetrics.marginDefault),
            child: Center(
              child: Text(
                title,
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
