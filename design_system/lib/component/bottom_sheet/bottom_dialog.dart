import 'package:flutter/material.dart';

import '../../theme/colors.dart';
import '../../theme/metrics.dart';

class BottomDialog extends StatelessWidget {
  const BottomDialog({Key key, this.child}) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: ProjectColors.cardBackgroundGradient,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(ProjectMetrics.cardRadius),
          topRight: Radius.circular(ProjectMetrics.cardRadius),
        ),
        shape: BoxShape.rectangle,
      ),
      child: child,
    );
  }
}
