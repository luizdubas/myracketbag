import 'package:flutter/material.dart';

import '../../theme/colors.dart';
import '../../theme/metrics.dart';

class BaseListCard extends StatelessWidget {
  const BaseListCard({Key key, this.child}) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(
        ProjectMetrics.marginDefault,
        ProjectMetrics.marginDefault,
        ProjectMetrics.marginDefault,
        0,
      ),
      child: Container(
        decoration: BoxDecoration(
          gradient: ProjectColors.cardBackgroundGradient,
          borderRadius: BorderRadius.all(
            Radius.circular(ProjectMetrics.cardRadius),
          ),
          shape: BoxShape.rectangle,
        ),
        child: Padding(
          padding: EdgeInsets.all(ProjectMetrics.marginDefault),
          child: child,
        ),
      ),
    );
  }
}
