import 'package:flutter/material.dart';

import '../../theme/colors.dart';
import '../../theme/text_styles.dart';

class DarkTextField extends StatelessWidget {
  const DarkTextField({
    Key key,
    this.controller,
    this.keyboardType,
    this.hintText,
    this.prefixIcon,
    this.validator,
  }) : super(key: key);

  final TextEditingController controller;
  final TextInputType keyboardType;
  final String hintText;
  final Widget prefixIcon;
  final FormFieldValidator<String> validator;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        autofocus: true,
        controller: controller,
        keyboardType: keyboardType,
        style: ProjectTextStyles.roboto24MediumLight,
        validator: validator,
        decoration: InputDecoration(
          hintStyle: ProjectTextStyles.roboto24MediumLightHint,
          hintText: hintText,
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          fillColor: ProjectColors.darkTextFieldFillColor,
          filled: true,
          prefixIcon: prefixIcon,
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: ProjectColors.darkTextFieldFocusedBorderColor,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: ProjectColors.darkTextFieldBorderColor,
            ),
          ),
        ),
      ),
    );
  }
}
