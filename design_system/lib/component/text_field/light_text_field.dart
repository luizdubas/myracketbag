import 'package:flutter/material.dart';

import '../../theme/colors.dart';
import '../../theme/text_styles.dart';

class LightTextField extends StatelessWidget {
  const LightTextField({
    Key key,
    this.controller,
    this.keyboardType,
    this.hintText,
    this.prefixIcon,
    this.focusNode,
    this.validator,
    this.autofocus = false,
    this.obscureText = false,
  }) : super(key: key);

  final bool autofocus;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final String hintText;
  final Widget prefixIcon;
  final FocusNode focusNode;
  final FormFieldValidator<String> validator;
  final bool obscureText;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        focusNode: focusNode,
        autofocus: autofocus,
        obscureText: obscureText,
        controller: controller,
        keyboardType: keyboardType,
        style: ProjectTextStyles.roboto24MediumLight,
        validator: validator,
        decoration: InputDecoration(
          hintStyle: ProjectTextStyles.roboto24MediumLightHint,
          hintText: hintText,
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          fillColor: ProjectColors.lightTextFieldFillColor,
          filled: true,
          prefixIcon: prefixIcon,
          errorStyle: ProjectTextStyles.errorStyle,
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: ProjectColors.errorTextFieldBorderColor,
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: ProjectColors.focusedErrorTextFieldBorderColor,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: ProjectColors.lightTextFieldFocusedBorderColor,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              width: 0,
              color: ProjectColors.lightTextFieldBorderColor,
            ),
          ),
        ),
      ),
    );
  }
}
