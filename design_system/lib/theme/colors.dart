import 'package:flutter/material.dart';

// ignore_for_file: avoid_classes_with_only_static_members
class ProjectColors {
  //Hex: #141A26
  static const backgroundColor = Color.fromARGB(255, 20, 26, 38);

  //Hex: #F20958
  static const accentColor = Color.fromARGB(255, 242, 9, 88);

  //Hex: #63193E
  static const highlightColor = Color.fromARGB(255, 99, 25, 62);

  //Hex: #DAE3E5
  static const lightTextColor = Color.fromARGB(255, 255, 255, 255);

  //Hex: #F20958
  static const darkTextColor = accentColor;

  //Hex: #DAE3E5
  static const borderColor = lightTextColor;

  //Hex: #DAE3E5
  static final lightTextHintColor = lightTextColor.withOpacity(0.5);

  //Hex: #DAE3E5
  static const darkEnabledIcon = backgroundColor;

  //Hex: #0C131C 40% opacity
  static const darkTextFieldFillColor = Color.fromARGB(102, 12, 19, 28);

  //Hex: #0C131C 50% opacity
  static const darkTextFieldBorderColor = Color.fromARGB(127, 12, 19, 28);

  //Hex: #0C131C
  static const darkTextFieldFocusedBorderColor = backgroundColor;

  //Hex: #DAE3E5
  static const lightEnabledIcon = lightTextColor;

  //Hex: #DAE3E5 40% opacity
  static final lightTextFieldFillColor = lightTextColor.withOpacity(0.4);

  //Hex: #DAE3E5 50% opacity
  static final lightTextFieldBorderColor = lightTextColor.withOpacity(0.5);

  //Hex: #DAE3E5
  static const lightTextFieldFocusedBorderColor = lightTextColor;

  static final errorColor = Colors.red.shade900;

  static final errorTextFieldBorderColor = Colors.red.withOpacity(0.54);

  static final focusedErrorTextFieldBorderColor = Colors.red.shade900;

  //Hex: #A1C6EA
  static const cardBackgroundGradientStart = ProjectColors.accentColor;

  //Hex: #AC0073
  static const cardBackgroundGradientEnd = Color.fromARGB(255, 172, 0, 115);

  static final cardBackgroundGradient = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: <Color>[
      ProjectColors.cardBackgroundGradientStart,
      ProjectColors.cardBackgroundGradientEnd
    ],
    tileMode: TileMode.clamp,
  );

  //Hex: #FFFFFD
  static const tabBackgroundGradientStart = Color.fromARGB(255, 255, 255, 253);

  //Hex: #CCCDD1
  static const tabBackgroundGradientEnd = Color.fromARGB(255, 204, 205, 209);

  static final tabBackgroundGradient = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: <Color>[
      ProjectColors.tabBackgroundGradientStart,
      ProjectColors.tabBackgroundGradientEnd
    ],
    tileMode: TileMode.clamp,
  );

  //Hex: #141A26
  static const scrollHighlighGradientStart = ProjectColors.backgroundColor;

  //Hex: #141A26 0% opacity
  static final scrollHighlighGradientEnd =
      ProjectColors.backgroundColor.withOpacity(0);
}
