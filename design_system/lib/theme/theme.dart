import 'package:flutter/material.dart';

import 'colors.dart';

final projectTheme = ThemeData(
  accentColor: ProjectColors.highlightColor,
  highlightColor: ProjectColors.highlightColor,
);
