class ProjectMetrics {
  // Button
  static const buttonBig = 80.0;
  static const buttonNormal = 60.0;

  // Border
  static const borderDefault = 2.0;

  // Corner radius
  static const tabButtonRadius = 30.0;
  static const cardRadius = 20.0;
  static const textRadius = 8.0;

  // Margin
  static const marginBetween = 4.0;
  static const marginSlim = 8.0;
  static const marginVertical = 20.0;
  static const marginDefault = 24.0;
  static const marginLarge = 48.0;
}
