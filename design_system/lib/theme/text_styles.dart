import 'package:flutter/material.dart';

import 'colors.dart';

// ignore_for_file: avoid_classes_with_only_static_members
class ProjectTextStyles {
  static const roboto36MediumDark = TextStyle(
    color: ProjectColors.darkTextColor,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w500,
    fontSize: 36.0,
    decoration: TextDecoration.none,
  );

  static const roboto28MediumDark = TextStyle(
    color: ProjectColors.darkTextColor,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w500,
    fontSize: 28.0,
    decoration: TextDecoration.none,
  );

  static const roboto28LightDark = TextStyle(
    color: ProjectColors.darkTextColor,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w300,
    fontSize: 28.0,
    decoration: TextDecoration.none,
  );

  static const roboto24MediumDark = TextStyle(
    color: ProjectColors.darkTextColor,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w500,
    fontSize: 24.0,
    decoration: TextDecoration.none,
  );

  static const roboto20MediumDark = TextStyle(
    color: ProjectColors.darkTextColor,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w500,
    fontSize: 20.0,
    decoration: TextDecoration.none,
  );

  static const roboto20RegularDark = TextStyle(
    color: ProjectColors.darkTextColor,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w400,
    fontSize: 20.0,
    decoration: TextDecoration.none,
  );

  static const roboto14BoldLight = TextStyle(
    color: ProjectColors.lightTextColor,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w700,
    fontSize: 14.0,
    decoration: TextDecoration.none,
  );

  static const roboto28MediumLight = TextStyle(
    color: ProjectColors.lightTextColor,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w500,
    fontSize: 28.0,
    decoration: TextDecoration.none,
  );

  static const roboto24MediumLight = TextStyle(
    color: ProjectColors.lightTextColor,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w500,
    fontSize: 24.0,
    decoration: TextDecoration.none,
  );

  static final roboto24MediumLightHint = TextStyle(
    color: ProjectColors.lightTextHintColor,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w500,
    fontSize: 24.0,
    decoration: TextDecoration.none,
  );

  static const roboto20MediumLight = TextStyle(
    color: ProjectColors.lightTextColor,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w500,
    fontSize: 20.0,
    decoration: TextDecoration.none,
  );

  static const roboto14RegularLight = TextStyle(
    color: ProjectColors.lightTextColor,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w400,
    fontSize: 14.0,
    decoration: TextDecoration.none,
  );

  static const roboto14LightLight = TextStyle(
    color: ProjectColors.lightTextColor,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w300,
    fontSize: 14.0,
    decoration: TextDecoration.none,
  );

  static final errorStyle = TextStyle(
    color: ProjectColors.errorColor,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w500,
    fontSize: 14.0,
    decoration: TextDecoration.none,
  );
}
