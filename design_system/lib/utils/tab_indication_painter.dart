import 'dart:math';

import 'package:flutter/material.dart';
import '../theme/colors.dart';

class TabIndicationPainter extends CustomPainter {
  Paint painter;
  final double dxTarget;
  final double dxEntry;
  final double radius;
  final double dy;

  final PageController pageController;

  TabIndicationPainter(
      {this.dxTarget = 141.5,
      this.dxEntry = 16.0,
      this.radius = 16.0,
      this.dy = 16.0,
      this.pageController})
      : super(repaint: pageController) {
    painter = Paint()
      ..color = ProjectColors.cardBackgroundGradientStart
      ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    final pos = pageController.position;
    final fullExtent =
        (pos.maxScrollExtent - pos.minScrollExtent + pos.viewportDimension);

    final pageOffset = pos.extentBefore / fullExtent;

    final left2right = dxEntry < dxTarget;
    final entry = Offset(left2right ? dxEntry : dxTarget, dy);
    final target = Offset(left2right ? dxTarget : dxEntry, dy);

    final path = Path();
    path.addArc(
        Rect.fromCircle(center: entry, radius: radius), 0.5 * pi, 1 * pi);
    path.addRect(Rect.fromLTRB(entry.dx, dy - radius, target.dx, dy + radius));
    path.addArc(
        Rect.fromCircle(center: target, radius: radius), 1.5 * pi, 1 * pi);

    canvas.translate(size.width * pageOffset, 0.0);
    canvas.drawPath(path, painter);
  }

  @override
  bool shouldRepaint(TabIndicationPainter oldDelegate) => true;
}
